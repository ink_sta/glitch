from pyglet.window import key as keys
import cocos
import math
from cocos.actions import *

class MainScene(cocos.scene.Scene):
    def __init__(self):
        super().__init__()
        self.scroller = cocos.layer.ScrollingManager()
        test_layer = cocos.tiles.load('trytomap2.tmx')['Слой тайлов 2']
        self.scroller.add(test_layer)
        game_layer = GameLayer()
        self.scroller.add(game_layer)
        self.add(self.scroller)
        #game_layer.set_view(x=400,y=300,w=800,h=600,viewport_ox=0,viewport_oy=0)

class GameLayer(cocos.layer.ScrollableLayer):
    is_event_handler = True
    def __init__(self):
        super().__init__()
        sprite = cocos.sprite.Sprite('boyyyyyyyyyyyy.png')
        self.sprite = sprite
        self.add(sprite)
        self.pressed=set()
        self.schedule(self.move)
        self.directions={
            (True, False,False,False): MoveBy((0,15), 0.1),
            (True, False,False,True): MoveBy((-10.5,10.5), 0.1),
            (True, True, False,False): MoveBy((10.5,10.5), 0.1),
            (False,True, False,False): MoveBy((15,0), 0.1),
            (False,True, True, False): MoveBy((10.5,-10.5), 0.1),
            (False,False,True, False): MoveBy((0,-15), 0.1),
            (False,False,True, True): MoveBy((-10.5,-10.5), 0.1),
            (False,False,False,True): MoveBy((-15,0), 0.1),
            }



    def on_key_press(self, key, modifiers):
        self.pressed.add(key)

    def on_key_release(self, key, modifiers):
        self.pressed.remove(key)

    def move(self, dt):
        direction=(keys.W in self.pressed,keys.D in self.pressed,keys.S in self.pressed,keys.A in self.pressed)
        go=self.directions.get(direction, None)
        if go:
            self.sprite.do(go)
        self.parent.set_focus(self.sprite.x, self.sprite.y)

"""
    def on_mouse_press(self, x, y, buttons, modifiers):
        x, y = self.map_layer.get_virtual_coordinates(x, y)
        self.x1=x
        self.y1=y
        
        
"""  


cocos.director.director.init()
cocos.director.director.run(MainScene())

